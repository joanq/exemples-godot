class_name Mob
extends RigidBody2D


var player : Area2D
var velocity : Vector2

@export var bullet_scene: PackedScene


# Called when the node enters the scene tree for the first time.
func _ready():
	var mob_types = $AnimatedSprite2D.sprite_frames.get_animation_names()
	$AnimatedSprite2D.play(mob_types[randi() % mob_types.size()])


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass	

func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()


func start_movement(_player: Area2D, mob_spawn_location: PathFollow2D):
	player = _player
	mob_spawn_location.progress_ratio = randf()
	
	var direction = mob_spawn_location.rotation + PI / 2
	
	position = mob_spawn_location.position
	
	direction += randf_range(-PI / 4, PI / 4)
	rotation = direction
	
	velocity = Vector2(randf_range(150.0, 250.0), 0.0)
	linear_velocity = velocity.rotated(direction)


func fire():
	var bullet : RigidBody2D = bullet_scene.instantiate()
	bullet.position = position
	bullet.rotation = rotation + randf_range(-PI/4, PI/4)
	bullet.linear_velocity = Vector2(300, 0).rotated(bullet.rotation)
	get_tree().root.add_child(bullet)


func _on_fire_timer_timeout():
	fire()
