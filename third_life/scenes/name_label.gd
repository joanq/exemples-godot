extends Label


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var parent:CharacterBody2D = get_parent()
	global_position = parent.get_global_position()
	global_position.x -= get_size().x / 2
