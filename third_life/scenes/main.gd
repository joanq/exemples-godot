extends Node

const PORT=9797
@export var player : PackedScene
var player_name = "unnamed"


func _on_init_screen_host(pname):
	player_name = pname
	$ChatUI.active = true
	
	var peer = ENetMultiplayerPeer.new()
	var error = peer.create_server(PORT)
	print(error)
	multiplayer.multiplayer_peer = peer

	multiplayer.peer_disconnected.connect(remove_player)

	load_game()


func _on_init_screen_join(ip, pname):
	player_name = pname
	$ChatUI.active = true

	var peer = ENetMultiplayerPeer.new()
	var error = peer.create_client(ip, PORT)
	print(error)
	multiplayer.multiplayer_peer = peer

	multiplayer.connected_to_server.connect(load_game)
	multiplayer.server_disconnected.connect(server_offline)

func load_game():
	$InitScreen.visible = false
	$ChatUI.active = true
	
	add_player.rpc_id(1, multiplayer.get_unique_id(), player_name)


@rpc("any_peer","call_local") # Add "call_local" to spawn a player from the server
func add_player(id, pname):
	var player_instance = player.instantiate()
	
	player_instance.name = str(id)
	$Players.add_child(player_instance)
	$ChatUI.talk("Player connected: "+pname)
	player_instance.set_player_name(pname)
	player_instance.position.x+=randi_range(-20,20)
	player_instance.position.y+=randi_range(-20,20)


@rpc("any_peer")
func remove_player(id):
	if $Players.get_node(str(id)):
		$Players.get_node(str(id)).queue_free()
		print("remove_player")


func server_offline():
	$InitScreen.visible = true

