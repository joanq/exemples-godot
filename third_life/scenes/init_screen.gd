extends CanvasLayer

signal host(player_name)
signal join(ip, player_name)


func _on_join_button_pressed():
	var player_name = %NameEdit.get_text().strip_edges().strip_escapes()
	if player_name!="":
		emit_signal("join", %IPEdit.get_text(), player_name)
	else:
		%NameEdit.grab_focus()


func _on_host_button_pressed():
	var player_name = %NameEdit.get_text().strip_edges().strip_escapes()
	if player_name!="":
		emit_signal("host", player_name)
	else:
		%NameEdit.grab_focus()
