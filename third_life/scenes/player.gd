extends CharacterBody2D
class_name Player

const MAX_SPEED = 125
const ACCELERATION_SMOOTHING = 25

var active = true
var player_name = "unnamed"

func _enter_tree():
	set_multiplayer_authority(name.to_int())
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not is_multiplayer_authority(): return
	
	$NameLabel.set_text(get_node("../..").player_name)
	
	var movement_vector = get_movement_vector()
	var direction = movement_vector.normalized()
	var target_velocity = direction * MAX_SPEED
	
	set_animation(movement_vector)
	velocity = velocity.lerp(target_velocity, 1-exp(-delta*ACCELERATION_SMOOTHING))
	move_and_slide()

func get_movement_vector():
	var x_movement = 0
	var y_movement = 0
	if Input.is_action_just_pressed("text"):
		active = not active
	if active:
		x_movement = Input.get_action_strength("right") - Input.get_action_strength("left")
		y_movement = Input.get_action_strength("down") - Input.get_action_strength("up")
	return Vector2(x_movement, y_movement)

func set_animation(movement_vector):
	if movement_vector.y > 0:
		$AnimatedSprite2D.animation = "down"
	elif movement_vector.y < 0:
		$AnimatedSprite2D.animation = "up"
	elif movement_vector.x > 0:
		$AnimatedSprite2D.animation = "right"
	elif movement_vector.x < 0:
		$AnimatedSprite2D.animation = "left"
	
	if movement_vector.length() > 0:
		$AnimatedSprite2D.play()
	else:
		$AnimatedSprite2D.stop()

func get_player_name():
	return player_name
	
func set_player_name(pname):
	player_name = pname
	$NameLabel.set_text(pname)
	print(pname)
