extends CanvasLayer

@onready var input_text: LineEdit = %InputText 
@onready var output_text: TextEdit = %OutputText
var active = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("text") and active:
		if input_text.is_editable():
			talk.rpc_id(1, get_node("..").player_name+": "+input_text.get_text())
			input_text.clear()
			input_text.set_editable(false)
		else:
			input_text.set_editable(true)
			input_text.grab_focus()

@rpc("any_peer","call_local","reliable")
func talk(text):
	var players = get_tree().get_nodes_in_group("player")
	for player in players:
		add_text.rpc_id(int(str(player.name)), text)

@rpc("reliable","call_local")
func add_text(text):
	output_text.set_text(output_text.get_text()+"\n"+text)
