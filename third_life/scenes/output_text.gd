extends TextEdit


func _on_text_changed():
	set_v_scroll(get_line_count()-1)


func _on_text_set():
	set_v_scroll(get_line_count()-1)
